class CompressImagesService

  attr_accessor :group, :zipfile_path

  def initialize(group:)
    @group = group
    @zipfile_path = "/tmp/#{Time.now.to_formatted_s(:number) }.zip"
  end

  def call
    Zip::File.open(zipfile_path, Zip::File::CREATE) do |zipfile|
      fill_file(zipfile)
    end
  end

  private

  def fill_file(zipfile)
    zip_images(zipfile)
    create_readme(zipfile)
  end

  def zip_images(zipfile)
    Image.where(group: group).each do |image|
      url = image.file.url
      zipfile.add(File.basename(url), "public" + url)
    end
  end

  def create_readme(zipfile)
    zipfile.get_output_stream("README") do |os|
      os.write "This file is generated on #{Time.now.to_formatted_s(:short)}"
    end
  end
end
