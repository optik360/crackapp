class VideoCutService

  attr_accessor :video_path, :images, :group

  def initialize(file:)
    @video_path  = file.path
    @group = file.original_filename + ' - ' + Time.now.to_formatted_s(:short)
    @images = []
  end

  def call
    split_video do |file|
      image = create_image_from_file(file)
      move_image_from_tmp_to_upload_dir(file, image)
    end
    return images
  end

  private

  def split_video
    Dir.mktmpdir do |dir|
      `avconv -i #{video_path} -r 1 #{dir}/%03d.jpg`
      Dir["#{dir}/*"].sort.each { |file| yield(file) }
    end
  end

  def move_image_from_tmp_to_upload_dir(file, image)
    `mkdir -p #{Rails.root}/public/uploads/image/file/#{image.id}`
    `mv #{file} #{Rails.root}/public/uploads/image/image/#{image.id}/`
  end

  def create_image_from_file(file)
    Image.create(file: File.open(file), group: group).tap do |image|
      images << image
    end
  end
end
