class ImagesController < ApplicationController
  def index
    sql = "ST_Contains(surface, ST_GeomFromText('POINT(:x :y)'))"
    render json: Image.where(sql, x: x, y: y)
  end

  def create
    group = Time.now.to_i
    images = params[:files].map { |image| upload_image(image, group) }
    render json: images, status: :created
  end

  def zip
    compress_images_service.call
    send_file(compress_images_service.zipfile_path)
  end

  private

  def x
    params[:x].to_i
  end

  def y
    params[:y].to_i
  end

  def images_params
    params.require(:item).permit(:file, :group)
  end

  def compress_images_service
    @compress_images_service ||=
      CompressImagesService.new(group: params[:group])
  end

  def video_cut_service(file)
    @video_cut_service ||= VideoCutService.new(file: file)
  end

  def upload_image(file, group)
    if file.content_type.starts_with?('image')
      create_image(file, group)
    else
      video_cut_service(file).call
    end
  end

  def create_image(file, group)
    Image.create(file: file, group: group)
  end
end
