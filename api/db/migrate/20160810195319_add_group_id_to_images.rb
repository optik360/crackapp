class AddGroupIdToImages < ActiveRecord::Migration[5.0]
  def change
    add_column :images, :group, :string
  end
end
