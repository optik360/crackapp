class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.geometry :surface

      t.timestamps
    end
  end
end
