mapboxgl.accessToken = 'pk.eyJ1IjoiZG91Z3VpIiwiYSI6IlZRNEY0QnMifQ.a0MDJozU8vlq_tvxZKYIyg';

getCoordinates = function (coords)  {
  return [
    [coords.topLeft.lat, coords.topLeft.lng],
    [coords.topRight.lat, coords.topRight.lng],
    [coords.bottomRight.lat, coords.bottomRight.lng],
    [coords.bottomLeft.lat, coords.bottomLeft.lng],
  ]
}

openMap = function (options) {
  var coordinates = getCoordinates(options.coords);
  var center = [options.coords.center.lat, options.coords.center.lng];
  var imagePath = options.imagePath;
  console.log(coordinates);

  var mapStyle = {
    "version": 8,
    "sources": {
      "satellite": {
        "type": "raster",
        "url": "mapbox://mapbox.satellite"
      },
      "overlay": {
        "type": "image",
        "url": imagePath,
        "coordinates": coordinates
      }
    },
    "layers": [
    {
      "id": "satellite",
      "type": "raster",
      "source": "satellite"
    },
    {
      "id": "overlay",
      "source": "overlay",
      "type": "raster"
    }
    ]
  };

  var map = new mapboxgl.Map({
    container: 'map',
    zoom: 18,
    center: center,
    style: mapStyle,
    hash: false
  });
}
