jQuery(function ($) {
  function createSocketStitch() {
    var host = "ws://localhost:9091/ws";
    var socket = new WebSocket(host);

    // event handlers for websocket
    if(socket){
      socket.onopen = function(){
        console.log("connection opened....");
      }

      socket.onmessage = function(response){
        var data = JSON.parse(response.data);

        if(data.status == 200) {
          var groupId = $('#group-id').val();

          openMap({ imagePath: '/orthophotos/' + groupId + '.png', coords: data.data });
          $('#ortho-img')[0].src = '/orthophotos/' + groupId + '.png';
          $('#collapse-ortho').show();
          $('#loader').removeClass('loader');
        } else {
          alert(response.data);
        }
      }

      socket.onclose = function(){
        console.log("connection closed....");
      }
    } else {
      console.log("invalid socket");
    }

    return socket;
  }

  if (!("WebSocket" in window)) {
    alert("Your browser does not support web sockets");
  } else {
    setup();
  }

  $("input#get-images").click(function () {
    var formData = new FormData();
    $('#loader').html("").addClass('loader');
    $.each($('#file')[0].files, function (_, file) {
      formData.append('files[]', file);
    });

    $.ajax({
      url: '/images',
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      success: function (data) {
        $('#loader').removeClass('loader');

        var parsed_data = $.parseJSON(JSON.stringify(data));
        var groupId = parsed_data[0].group
        $('#group-id').val(groupId);

        var newLink = $("<a />", {
          href: '/zip?group=' + groupId,
          text: "Download zip file"
        });

        $('#zip').html(newLink)
          for (var i = 0; i < parsed_data.length; i++) {
            var img = $('<img />', {
              id: 'image',
              src: parsed_data[i].file.url,
              alt: 'img',
              width: '200px',
              onclick: 'imgClick(this)'
            });
            img.appendTo($('#images'));
          }
        $("#collapse-original").css('display', 'block');
        $("#sendImage").prop('disabled', false);
        $("#get-ortho").prop('disabled', false);
      }
    });
  });

  $("input#get-ortho").click(function () {
    var socket = createSocketStitch();

    socket.onopen = function(){
      var groupId = $('#group-id').val();
      var data = { command: 'stitch', group_id: groupId};
      var str = JSON.stringify(data);
      socket.send(str);

      $('#loader').html("").addClass('loader');
    }
  });

  function setup() {
    // Note: You have to change the host var
    // if your client runs on a different machine than the websocket server

    var host = "ws://localhost:9090/ws";
    var socket = new WebSocket(host);
    var nbTask = 0;
    var bar = document.getElementById("myBar");
    var $btnSend = $("#sendImage");
    $("#sendImage").prop('disabled', true);
    $("#get-ortho").prop('disabled', true);

    // event handlers for UI
    $btnSend.on('click', function () {
      $('#output').html('');
      $('#loader').addClass('loader');
      var children = $('#images').children();
      for (var i = 0; i < children.length; i++) {
        var text = children[i].src;
        if (text == "") {
          return;
        }
        socket.send(JSON.stringify({
          radius: $('#radius').val(),
          points: $('#points').val(),
          photo: text
        }));
      }
    });

    // event handlers for websocket
    if (socket) {

      socket.onopen = function () {
        //alert("connection opened....");
      }

      socket.onmessage = function (msg) {
        showServerResponse(msg.data);
      }

      socket.onclose = function () {
        alert("connection closed....");
      }

    } else {
      console.log("invalid socket");
    }

    function showServerResponse(txt) {
      if (isNaN(txt)) {
        $("#collapse-edited").css('display', 'block');
        var p = "<img id='image' width='30%' onClick='imgClick(this)' src='data:image/png;base64," + txt + "' />";
        $('#loader').removeClass('loader');
        $('#output').append(p);
        nbTask = 0;
        $('#myProgress').css('display', 'none');
      } else {
        if (nbTask == 0) {
          nbTask = parseInt(txt)
        } else {
          $('#myProgress').css('display', 'block');
          var pourcent = Math.round((parseInt(txt) / nbTask) * 100) + '%';
          bar.style.width = pourcent;
          $('#label').html(pourcent);
        }
      }
    }
  }
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $("#sendImage").prop('disabled', false);
      $("#get-ortho").prop('disabled', false);
      $('#image').attr('src', e.target.result).css('display', 'block');
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function toDataUrl(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.responseType = 'blob';
  xhr.onload = function () {
    var reader = new FileReader();
    reader.onloadend = function () {
      callback(reader.result);
    }
    reader.readAsDataURL(xhr.response);
  };
  xhr.open('GET', url);
  xhr.send();
}


function imgClick(image) {
  $('#myModal').css('display',"block");
  $("#img01").attr("src",image.src);
}

$("#close").on('click', function () {
  console.log('asd')
    $("#myModal").css('display',"none");
});

$('#clear-btn').on('click',function(){
  $('#images').html('');
  $("#collapse-original").css('display', 'none ');
});
