# How to contribute

 - Create a new branch with `git checkout -b my_new_feature`;
 - Work and do commits with `git add . && git commit -m 'my commit message`;
 - Push your work with `git push`
 - Create new pull request by using the bitbucket interface. Give a description of what you did;
 - Wait for comment and approbation;
 - Have fun and take a break;
