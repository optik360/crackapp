require 'rails_helper'

feature 'Upload videos' do
  scenario 'The use upload a video and have images', js: true do
    visit '/'
    attach_file('file', "#{Rails.root}/spec/fixtures/video.mp4")
    click_on('get images')

    wait_for_ajax
    expect(page.all('#images img').length).to eq(3)
  end
end
