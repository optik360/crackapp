Rails.application.routes.draw do
  resources :images
  get 'zip', to: 'images#zip'
end
