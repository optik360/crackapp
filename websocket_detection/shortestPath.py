'''
But:            Calcule des chemins pour un point avec le rayon, les autres points et un tableau avec tout les couts (images)
Date:           2016-06-30
Createur:       Patrick Vigneault
Description:  	Recoit les parametre et applique l'algo de dijkstra, ensuite il renvoi un dict avec les chemins et les couts

Source
dijksra: http://code.activestate.com/recipes/119466-dijkstras-algorithm-for-shortest-paths/

'''

import math
from priodict import priorityDictionary
from joblib import Parallel, delayed
import operator, itertools
import cv2
import numpy as np
pathList = {}

def Dijkstra(img, start, end):

    D = {}  # dictionary of final distances
    P = {}  # dictionary of predecessors
    Q = priorityDictionary()   # est.dist. of non-final vert.
    Q[start] = 0
    
    for v in Q:
        G = {}
        G[v] = {}
        i = -1
        j = -1  
        neigX = 0
        neigY = 0   
        D[v] = Q[v]
        if v == end: break
        x,y = v.split('/',1)
        x = float(x)
        y = float(y)
 

        while i <= 1:
            while j <= 1:
                neigX = x + i
                neigY = y + j 

                if neigX < 0 or neigX >= len(img[0]) or neigY < 0 or neigY >= len(img):
                    G[v][str(x+i)+'/'+str(y+j)] = 255 
                else:
                    G[v][str(x+i)+'/'+str(y+j)] = img[y+j][x+i]
                j = j + 1
            j = -1
            i = i + 1
        for w in G[v]:
            vwLength = D[v] + G[v][w]
            if w in D:
                if vwLength < D[w]:
                    raise ValueError, \
  "Dijkstra: found better path to already-final vertex"
            elif w not in Q or vwLength < Q[w]:
                Q[w] = vwLength
                P[w] = v
    
    return (D,P)

def shortestPath(G,start,end):
    """
    Find a single shortest path from the given start vertex
    to the given end vertex.
    The input has the same conventions as Dijkstra().
    The output is a list of the vertices in order along
    the shortest path.
    """
    weight = 0
    D,P = Dijkstra(G,start,end)
    Path = []
    while 1:
        Path.append(end)
        if end == start: break
        end = P[end]
    Path.reverse()
    for point in Path:
        xr,yr = point.split('/',1)
        xr = int(float(xr))
        yr = int(float(yr))
        weight = weight + G[yr][xr]
    weight = weight/len(Path)
    return Path, weight
  
def findPath(i,radius,points,img):
    global pathList
    x,y = i.ravel()
    neighbors = [point for point in points if x - radius < point[0][0] and x + radius > point[0][0] and y - radius < point[0][1] and y + radius > point[0][1]]
    for e in neighbors:
        xe,ye = e.ravel()
        if str(xe)+'/'+str(ye)+' '+str(x)+'/'+str(y) not in pathList:             
            path, weight = shortestPath(img,str(x)+'/'+str(y),str(xe)+'/'+str(ye))
            pathList[str(x)+'/'+str(y)+' '+str(xe)+'/'+str(ye)] = weight, path
    return pathList 
  
  
  