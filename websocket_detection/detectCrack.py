from shortestPath import findPath
import cv2
import numpy as np
from joblib import Parallel, delayed

def markCrack(img,radius, points):
  kernel = np.ones((2,2),np.uint8)
  kernel2 = np.ones((3,3),np.uint8)
  weight = []
  meanWeight = 0

  img2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  img2 = cv2.bilateralFilter(img2,9,20,20)

  #Calcule du seuil pour le threshold 
  threshValue = cv2.mean(img2)[0] - (np.std(img2))

  #tresh = cv2.adaptiveThreshold(img2,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,31,25)
  ret,tresh = cv2.threshold(img2,threshValue,255,cv2.THRESH_BINARY_INV)

  #application de filtre pour enlever les bruits
  tresh = cv2.morphologyEx(tresh,cv2.MORPH_OPEN, kernel)
  tresh = cv2.morphologyEx(tresh,cv2.MORPH_CLOSE, kernel)

  #detection des points marquant dans l'image qui seront utilser pour les chemins
  dst = cv2.goodFeaturesToTrack(tresh,points,0.01,10,0,None,3,True,0.01)
  if dst is not None:   
    print len(dst) 
    #Calcule ds chemins avec parallel (multiprocesseur)
    pathList = (Parallel(n_jobs=2, verbose=50)(delayed(findPath)(i,radius,dst,img2) for i in dst))
    pathListFinal = {}
    for Dict in pathList:
        for k in Dict:
            pathListFinal[k] = Dict[k]

    for path in pathListFinal:
        weight.append(int(pathListFinal[path][0]))

    meanWeight = sum(weight) / len(weight)

    for path in pathListFinal:
        if pathListFinal[path][0] < meanWeight:
            for point in pathListFinal[path][1]:
                xr,yr = point.split('/',1)
                xr = int(float(xr))
                yr = int(float(yr))
                cv2.circle(img,(xr,yr),1,(255,0,255),-1)
    return img
  else:
    return img
