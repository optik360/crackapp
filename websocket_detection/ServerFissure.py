'''
But:            Cree un serveur quie vas permettre d'appliquer l'algorithme de detection de fissure avec les chemins minimaux
Date:           2016-07-20
Createur:       Patrick Vigneault
Description:  	Le serveur utilise les websockets afin de recevoir une image encoder en Base64 (chaine), prend cette image,
                la convertie pour opencv, et applique le traitement de chemin minimaux avec l'algo de djikstra, le calcule de chemin min
                se fait dans le fichier shortestPath.py. tout les print son envoyer au client. L'image est ensuite renvoyer au client en Base64
                qui la reaffiche par la suite
Source

tornado: http://www.tornadoweb.org/en/stable/
conversion base64 a opencv: http://stackoverflow.com/questions/34009524/python-alternate-way-to-covert-from-base64-string-to-opencv
Parallel: https://pythonhosted.org/joblib/parallel.html

'''
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
import tornado.autoreload
import base64
from PIL import Image
from io import BytesIO
import cv2
import numpy as np
import sys
from detectCrackTreshold import markCrack
import math
from priodict import priorityDictionary
import operator, itertools
import json
from tornado.escape import json_decode
import os

#change le stdout pour envoyer les messages au client via le websocket
class Output():
  old_std = None
  handler = None
  def __init__(self,old_std,handler):
    self.old_std = old_std
    self.handler = handler
  def write(self,text):
    if len(text) > 20 and len(text) < 100:
      text = text[20:]
      text = text[:15]
      text = ''.join(x for x in text if x.isdigit())
    self.handler.write_message(text)
  def flush(self):
    self.old_std.flush()

#Transforme une chaine Base64 en numpy array pour opencv
def readb64(base64_string):
  pimg = Image.open(BytesIO(base64.b64decode(base64_string)))
  return cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)

#websocket
class MainHandler(tornado.web.RequestHandler):

  def get(self):
    loader = tornado.template.Loader(".")
    self.write(loader.load("index.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):
  def check_origin(self, origin):
    return True

  def initialize(self):
      sys.stdout = Output(sys.stdout,self)

  def on_message(self, message):
    message = json_decode(message)
    photo = message['photo']
    photo = photo[photo.find(',')+1 :  ]
    img = readb64(photo)
    img = markCrack(img)
    img = cv2.imencode('.png',img)[1]
    str_img = base64.encodestring(img)
    self.write_message(str_img)

  def on_close(self):
    print 'connection closed...'


application = tornado.web.Application([
  (r'/ws', WSHandler),
  (r'/', MainHandler),
  (r"/(.*)", tornado.web.StaticFileHandler, {"path": "./resources"}),
])

if __name__ == "__main__":
    application.listen(9090)

    tornado.autoreload.start()
    for dir, _, files in os.walk('static'):
        [tornado.autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]

    tornado.ioloop.IOLoop.instance().start()
