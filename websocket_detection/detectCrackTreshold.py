# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------
# Copyright (c) 2016, ITMI, Samuel Austin, All rights reserved.
#-----------------------------------------------------------------------------
''' Étapes d'analyse
- low pass filtering pour enlever la texture (bilateral ou gaussian)
- watershed segmentation
- détection automatique de quel segment est celui qui contient l'asphalte
- application d'un algorithme de détection de fissure avec un mask

'''
import numpy as np
import cv2
from skimage import color
from math import sqrt

def markCrack(img):
  # Lecture de l'image
  img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
  img_gray_inv = 255 - img_gray

  # filtre passe-bas

  # http://docs.opencv.org/3.1.0/d4/d86/group__imgproc__filter.html#ga9d7064d478c95d60003cf839430737ed
  img_gray_inv_blur = cv2.bilateralFilter(img_gray_inv,9,75,75)

  # white space
  height, width = img_gray_inv_blur.shape
  blanc = np.ones((height, 10), np.uint8)*255

  # watershed segmentation

  # invert

  ret, img_gray_blur_inv_thresh = cv2.threshold(img_gray_inv, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
  ret, img_gray_blur_inv_blur_thresh = cv2.threshold(img_gray_inv_blur, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)


  # En premier, test sur l'image sans étape de blur, fonctionne vraiment mal
  #################################################
  # noise removal
  #kernel = np.ones((3,3),np.uint8)
  #opening = cv2.morphologyEx(img_gray_blur_inv_thresh, cv2.MORPH_OPEN, kernel, iterations = 2)
  #
  ## sure background area
  #sure_bg = cv2.dilate(opening, kernel, iterations=3)
  #
  ## Finding sure foreground area
  ## distortion pour séparer objets qui se touchent
  ## pas nécessaire dans notre cas
  ##dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
  ##ret, sure_fg = cv2.threshold(dist_transform, 0.7*dist_transform.max(), 255, 0)
  #
  ## tester nombres d'itérations différents
  #sure_fg = cv2.erode(opening, kernel, iterations = 1) 
  #
  ## Finding unknown region
  #sure_fg = np.uint8(sure_fg)
  #unknown = cv2.subtract(sure_bg,sure_fg)
  #
  #
  #side_by_side = np.hstack((sure_fg, blanc, sure_bg))
  #
  #cv2.imshow('image', side_by_side)
  #cv2.waitKey(0)
  #cv2.destroyAllWindows()
  #
  #
  ## Marker labelling
  #ret, markers = cv2.connectedComponents(sure_fg)
  #
  ## Add one to all labels so that sure background is not 0, but 1
  #markers = markers+1
  #
  ## Now, mark the region of unknown with zero
  #markers[unknown==255] = 0
  #
  #markers = cv2.watershed(img, markers)
  #
  ## génère une image de la segmentation
  #markers_uint8 = markers.astype(np.uint8)
  #markers_hist = cv2.equalizeHist(markers_uint8)
  #markers_jet = cv2.applyColorMap(markers_hist, cv2.COLORMAP_JET)
  #
  ## affiche les contours de la segmentation en rouge sur l'image originale
  #img_copy = np.copy(img)
  #img_copy[markers == -1] = [0,0,255]
  #
  #img_gray_blur_inv_thresh_rgb = color.gray2rgb(img_gray_blur_inv_thresh)
  #
  height, width, depth = img.shape
  hblanc = np.ones((height, 10, depth), np.uint8)*255
  vblanc = np.ones((10, width*2 + 10, depth), np.uint8)*255
  #
  #side_by_side1 = np.hstack((img, hblanc, img_gray_blur_inv_thresh_rgb))
  #side_by_side2 = np.hstack((markers_jet, hblanc, img_copy))
  #over_under = np.vstack((side_by_side1, vblanc, side_by_side2))
  #
  #cv2.imshow('image', over_under)
  #cv2.waitKey(0)
  #cv2.destroyAllWindows()


  # En deuxième, test sur l'image avec étape de blur, fonctionne beaucoup mieux
  #############################################################################


   # noise removal
  kernel = np.ones((3,3),np.uint8)
  opening = cv2.morphologyEx(img_gray_blur_inv_blur_thresh, cv2.MORPH_OPEN, kernel, iterations = 2)

  # sure background area
  sure_bg = cv2.dilate(opening, kernel, iterations=3)

  # Finding sure foreground area

  # distortion pour séparer objets qui se touchent
  # pas nécessaire dans notre cas
  #dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
  #ret, sure_fg = cv2.threshold(dist_transform, 0.7*dist_transform.max(), 255, 0)

  # tester nombres d'itérations différents
  sure_fg = cv2.erode(opening, kernel, iterations = 1) 

  # Finding unknown region
  sure_fg = np.uint8(sure_fg)
  unknown = cv2.subtract(sure_bg,sure_fg)


  # Marker labelling
  ret, markers = cv2.connectedComponents(sure_fg)

  # Add one to all labels so that sure background is not 0, but 1
  markers = markers+1

  # Now, mark the region of unknown with zero
  markers[unknown==255] = 0

  markers = cv2.watershed(img, markers)

  # génère une image de la segmentation
  markers_uint8 = markers.astype(np.uint8)
  markers_hist = cv2.equalizeHist(markers_uint8)
  markers_jet = cv2.applyColorMap(markers_hist, cv2.COLORMAP_JET)

  # affiche les contours de la segmentation en rouge sur l'image originale
  img_copy = np.copy(img)
  img_copy[markers == -1] = [0,0,255]

  img_gray_blur_inv_thresh_rgb = color.gray2rgb(img_gray_blur_inv_blur_thresh)

  # algorithme qui détecte quel segment est gris et enlève tous les autres segments
  # si deux segments dans des teintes de gris, pourrait conserver celui avec la 
  # plus grande surface
  #################################################################################

  # couleur moyenne pour chaque groupe

  couleur_moyenne = {}
  for marker_i in xrange(1, np.max(markers)+1):
      couleur_moyenne[marker_i] = np.mean(img[markers == marker_i], 0)

  # Zone d'asphalte
  # Coordonnée ci-dessous seulement valide pour l'image 03_025.png
  #imgAsphalte = img[300:400, 350:500]


  #cv2.imshow('image', imgAsphalte)
  #cv2.waitKey(0)
  #cv2.destroyAllWindows()

  # Moyennes du bleu, vert et rouge pour la zone d'asphalte
  #couleur_moyenne_asphalte = np.mean(np.mean(imgAsphalte, 0), 0)
  couleur_moyenne_asphalte = np.array([ 173.51773333,  168.51086667,  163.14466667])

  # Calculer la différence entre la couleur moyenne du groupe et la couleur de l'asphate
  couleur_difference = {}
  for marker_i in xrange(1, np.max(markers)+1):
      couleur_difference[marker_i] = sqrt((couleur_moyenne[marker_i][0] - couleur_moyenne_asphalte[0]) ** 2 + 
                                          (couleur_moyenne[marker_i][1] - couleur_moyenne_asphalte[1]) ** 2 + 
                                          (couleur_moyenne[marker_i][2] - couleur_moyenne_asphalte[2]) ** 2)

  # trouver la couleur la plus proche de gris asphalte
  marker_asphalte = min(couleur_difference, key=couleur_difference.get)

  # change la couleur pour indiquer le groupe trouvé
  # asphalte en bleu
  img_copy[markers == marker_asphalte] = [255,0,0]


  # trouver un moyen de combiner les petits segments à l'intérieur du segment 
  # de routes avec le segment de route
  # laisse faire pour l'instant, ce n'est peut-être pas une bonne idée parce
  # qu'il pourrait arrive que ce qui apparait à l'intérieur ne doit pas être
  # combiné
  ###########################################################################
  #
  ## générer une image de contour pour générer la hiérarchie
  ## http://docs.opencv.org/trunk/d3/d05/tutorial_py_table_of_contents_contours.html
  #
  #height, width, depth = img.shape
  #img_contours = np.zeros((height,width,1), np.uint8)
  #img_contours[markers == -1] = [255]
  #
  #cv2.imshow('image', img_contours)
  #cv2.waitKey(0)
  #cv2.destroyAllWindows()
  #
  ## contours et hierarchy
  ## In OpenCV, finding contours is like finding white object from black background. 
  ## So remember, object to be found should be white and background should be black.
  ## si ne fonctionne pas bien avec lorsqu'appliqué sur img_contours, utiliser markers_jet
  #img_contours2, contours, hierarchy = cv2.findContours(img_contours, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  #
  ##cv2.imshow('image', img_contours2)
  ##cv2.waitKey(0)
  ##cv2.destroyAllWindows()
  #
  ## dessiner les contours
  #img_avec_contours = np.copy(img)
  #cv2.drawContours(img_avec_contours, contours, -1, (0,0,255), 3)
  #
  #cv2.imshow('image', img_avec_contours)
  #cv2.waitKey(0)
  #cv2.destroyAllWindows()
  #
  ## trouver quel contour correspond au segment d'asphalte
  #
  ## par comparaison de couleur
  ##couleur_moyenne_contour = {}
  ##for i, cnt in enumerate(contours):
  ##    mask = np.zeros(img_gray.shape, np.uint8)
  ##    cv2.drawContours(mask,[cnt],0,255,-1)    
  ##    couleur_moyenne_contour[i] = cv2.mean(img, mask = mask)
  #
  #segment_mask = np.zeros(img_gray.shape, np.uint8)
  #segment_mask[markers == marker_asphalte] = [255]
  #
  ## test pour valider la sélection du mask sur segment
  #cv2.imshow('image', segment_mask)
  #cv2.waitKey(0)
  #cv2.destroyAllWindows()
  #
  #segment_pixelpoints = np.transpose(np.nonzero(segment_mask))
  #
  #
  #contour_pixelpoints = {}
  #contour_mask = {}
  #for i, cnt in enumerate(contours):
  #    contour_mask[i] = np.zeros(img_gray.shape, np.uint8)
  #    cv2.drawContours(contour_mask[i],[cnt],0,255,-1)    
  #    contour_pixelpoints[i] = np.transpose(np.nonzero(contour_mask[i]))
  #
  ## Affichage du nombre de pixels dans chaque contour
  #for i in contour_pixelpoints:
  #    print i, len(contour_pixelpoints[i])
  #
  ## affichage des deux masques cote à cote
  #
  #i = 245
  #segment_mask_rgb = color.gray2rgb(segment_mask)
  #contour_mask_rgb = color.gray2rgb(contour_mask[i])
  #
  #side_by_side1 = np.hstack((img, img_avec_contours))
  #side_by_side2 = np.hstack((segment_mask_rgb, contour_mask_rgb))
  #over_under = np.vstack((side_by_side1, side_by_side2))
  #
  #cv2.imshow('image', over_under)
  #cv2.waitKey(0)
  #cv2.destroyAllWindows()
  #
  ## comparaison des coordonnées de segment et contour pour trouver correspondant
  #
  ## Fait une intersection entre segment et contour. Retourne les points qui sont dans les deux.
  ## Pas optimisé, très lent. plusieurs minutes pour une petite image
  ## il faudrait faire la même chose sans transformer en set
  #matches = {}
  #for i in contour_pixelpoints:
  #    matches[i] = np.array([x for x in set(tuple(x) for x in segment_pixelpoints) & set(tuple(x) for x in  contour_pixelpoints[i])])
  #
  #
  #for i in matches:
  #    print i, len(matches[i])

  ###############################################################################

  # Exemple de l'application du code du test 4 avec le masque généré ci-dessus
  # pour traiter seulement l'asphalte

  # Utilise le mask pour analyser seulement la portion avec asphalte
  segment_mask = np.zeros(img_gray.shape, np.uint8)
  segment_mask[markers == marker_asphalte] = [255]

  def analyser(mask = None):
      img_gray_masked = cv2.bitwise_and(img_gray, img_gray, mask=mask)


      kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))

      img_gray_close = cv2.morphologyEx(img_gray_masked, cv2.MORPH_CLOSE, kernel)
      # utiliser la fonction substract pour ne pas que la soustraction wrap
      img_gray_bottom_hat = cv2.subtract(img_gray_close, img_gray_masked)
      img_gray_bottom_hat =  255 - img_gray_bottom_hat # Inverse les couleurs

      # Thresholding
      # threshold nécessite d'ajuster le threshdold manuellement (2e parametre)
      ret,img_gray_bottom_hat_thresh = cv2.threshold(img_gray_bottom_hat,int(0.9*255),256,cv2.THRESH_BINARY)
      img_gray_bottom_hat_thresh_inv = cv2.bitwise_not(img_gray_bottom_hat_thresh)


      # Traitements 2
      # -------------

      # Conversion HSV
      img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

      # HSV thresholding

      # thresholding pour sélectionner toutes les pixels de toutes les teintes,
      # et de saturation et luminance faible (fissures)
      lower_limit = np.array([0,0,0])
      upper_limit = np.array([255,120,120])
      img_hsv_thresh = cv2.inRange(img_hsv, lower_limit, upper_limit)



      # Combinaison de traitement 1 et 2
      # --------------------------------

      # Pas certain de l'algorithme de combinaison utlisé
      # développe une solution pour obtenir un résultat similaire à l'article

      resultat_final_inv = cv2.bitwise_and(img_gray_bottom_hat_thresh_inv, img_hsv_thresh)
  #    resultat_final = cv2.bitwise_not(resultat_final_inv)

      # Affichage des résultats side-by-side

      # conversion en couleur
  #    img_gray_bottom_hat_thresh_BGR = cv2.cvtColor(img_gray_bottom_hat_thresh, cv2.COLOR_GRAY2BGR)
  #    img_hsv_thresh_BGR = cv2.cvtColor(img_hsv_thresh, cv2.COLOR_GRAY2BGR)
  #    resultat_final_BGR = cv2.cvtColor(resultat_final, cv2.COLOR_GRAY2BGR)
  #    
  #    side_by_side = np.hstack((img, img_gray_bottom_hat_thresh_BGR, img_hsv_thresh_BGR, resultat_final_BGR))



      # Superpose rouge sur l'image d'origine

      # image rouge de la même taille que img
      height, width, depth = img.shape
      img_rouge = np.zeros((height,width,depth), np.uint8)
      img_rouge[:] = (0, 0, 255)

      # applique le mask à l'image
      # option 1 : soft
      #resultat_final_inv_3ch = cv2.merge((resultat_final_inv, resultat_final_inv, resultat_final_inv))
      #mask_rouge = cv2.bitwise_and(img_rouge, resultat_final_inv_3ch)
      #resultat_final_superpose = cv2.bitwise_or(img, mask_rouge)
      # option 2 : hard
      resultat_final_superpose = np.copy(img)
      idxs = resultat_final_inv == 255
      resultat_final_superpose[idxs] = (0, 0, 255)

      # Rendre les fissures plus visible
      # --------------------------------

      kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
      #kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(5,5))
      mask_dilatation = cv2.dilate(resultat_final_inv, kernel, iterations = 1)
      ret, mask_dilatation = cv2.threshold(mask_dilatation,127,256,cv2.THRESH_BINARY)

      # applique le mask à l'image
      # option 1 : soft
      #mask_dilatation__3ch = cv2.merge((mask_dilatation, mask_dilatation, mask_dilatation))
      #mask_dilatation_rouge = cv2.bitwise_and(img_rouge, mask_dilatation__3ch)
      #resultat_final_superpose_dilatation = cv2.bitwise_or(img, mask_dilatation_rouge) # pas exactement équiv à copier
      # option 2 : hard
      resultat_final_superpose_dilatation = np.copy(img)
      idxs = mask_dilatation == 255
      resultat_final_superpose_dilatation[idxs] = (0, 0, 255)
      return resultat_final_superpose_dilatation

  img_analyse_sans_mask = analyser()
  img_analyse_avec_mask = analyser(segment_mask)

  side_by_side = np.hstack((img_analyse_sans_mask, hblanc, img_analyse_avec_mask))

  return side_by_side

