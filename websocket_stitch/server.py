# -*- coding: utf-8 -*-

import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
import tornado.autoreload
from tornado.escape import json_decode
import subprocess
import os
import threading
import json

class openDroneMapThread(threading.Thread):
    def __init__(self, handler, group_id):
        threading.Thread.__init__(self)
        self.handler = handler
        self.group_id = group_id

    def run(self):
        try:
            group = self.group_id.replace(' ', '\ ')
            subprocess.check_output("mkdir -p /tmp/{0}/images".format(group), shell=True)
            subprocess.check_output("cp -R ./images/{0}/**/* /tmp/{1}/images".format(group, group), shell=True)
            subprocess.check_output("/opt/OpenDroneMap/run.sh --project-path /tmp/{0}".format(group), shell=True)
            subprocess.check_output("cp /tmp/{0}/odm_orthophoto/odm_orthophoto.png ./orthophotos/{0}.png".format(group, group), shell=True)
            subprocess.check_output("convert ./orthophotos/{0}.png -fuzz 1% -transparent white ./orthophotos/{0}.png".format(group, group), shell=True)

            coord = self.extract_coords(group)
            data = json.dumps(coord)

            self.handler.write_message({ 'status': 200, 'data': coord })
        except subprocess.CalledProcessError as e:
            self.handler.write_message({ 'status': 500, 'message': "Erreur pendant la création de l'orthophoto : \n\n" + e.output })

    def extract_coords(self, group):
        coords = {};
        coord_path = "/tmp/{0}/odm_orthophoto/coord".format(group)
        tif_path = "/tmp/{0}/odm_orthophoto/odm_orthophoto.tif".format(group)
        subprocess.check_output('gdalinfo {0} | grep -E "^Upper Left" | grep -o -E "  ([0-9.]*), ([0-9.]*)" > {1}'.format(tif_path, coord_path), shell=True)
        arr = subprocess.check_output("cat {0} | gdaltransform -s_srs EPSG:32617 -t_srs EPSG:4326".format(coord_path), shell=True).split(' ')
        topLeft = { 'lat': float(arr[0]), 'lng': float(arr[1]) }

        subprocess.check_output('gdalinfo {0} | grep -E "^Lower Left" | grep -o -E "  ([0-9.]*), ([0-9.]*)" > {1}'.format(tif_path, coord_path), shell=True)
        arr = subprocess.check_output("cat {0} | gdaltransform -s_srs EPSG:32617 -t_srs EPSG:4326".format(coord_path), shell=True).split(' ')
        bottomLeft = { 'lat': float(arr[0]), 'lng': float(arr[1]) }

        subprocess.check_output('gdalinfo {0} | grep -E "^Lower Right" | grep -o -E "  ([0-9.]*), ([0-9.]*)" > {1}'.format(tif_path, coord_path), shell=True)
        arr = subprocess.check_output("cat {0} | gdaltransform -s_srs EPSG:32617 -t_srs EPSG:4326".format(coord_path), shell=True).split(' ')
        bottomRight = { 'lat': float(arr[0]), 'lng': float(arr[1]) }

        subprocess.check_output('gdalinfo {0} | grep -E "^Upper Right" | grep -o -E "  ([0-9.]*), ([0-9.]*)" > {1}'.format(tif_path, coord_path), shell=True)
        arr = subprocess.check_output("cat {0} | gdaltransform -s_srs EPSG:32617 -t_srs EPSG:4326".format(coord_path), shell=True).split(' ')
        topRight = { 'lat': float(arr[0]), 'lng': float(arr[1]) }

        subprocess.check_output('gdalinfo {0} | grep -E "Center" | grep -o -E "  ([0-9.]*), ([0-9.]*)" > {1}'.format(tif_path, coord_path), shell=True)
        arr = subprocess.check_output("cat {0} | gdaltransform -s_srs EPSG:32617 -t_srs EPSG:4326".format(coord_path), shell=True).split(' ')
        center = { 'lat': float(arr[0]), 'lng': float(arr[1]) }

        return { 'bottomLeft': bottomLeft, 'topLeft': topLeft,
                'topRight': topRight,
                'bottomRight': bottomRight, 'center': center }

class WSHandler(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def on_message(self, message):
        message = json_decode(message)
        openDroneMapThread(self, message['group_id']).start()

    def on_close(self):
      print 'connection closed...'

application = tornado.web.Application([
        (r'/ws', WSHandler),
        (r"/(.*)", tornado.web.StaticFileHandler, {"path": "./resources"}),
    ])

if __name__ == "__main__":
    application.listen(9091)

    tornado.autoreload.start()
    for dir, _, files in os.walk('static'):
        [tornado.autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]

    tornado.ioloop.IOLoop.instance().start()
